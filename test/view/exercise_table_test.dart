import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:workout/view/exercises/exercise_table/exercise_table_row_cell.dart';

void main() {
  group('Tests for exercise table row cell', () {
    Widget widgetUnderTest;

    setUp(() {
      widgetUnderTest = MaterialApp(
        home: Scaffold(
          body: Container(
            child: ExerciseTableRowCell(
              textInputType: TextInputType.text,
              text: 'Bardzo, bardzo dlugi tekst ktory nie miesci sie nawet w dwoch liniach',
              exerciseRowData: {
                "exerciseNumber": '1',
                "index": '0',
                "category": 'test',
              },
            ),
          ),
        ),
      );
    });

    testWidgets('should show multiline text', (WidgetTester tester) async {
      //Given
      await tester.pumpWidget(widgetUnderTest);
      //When
      final height = tester.getSize(find.byType(TextFormField)).height;
      //Then
      expect(height > 48, true);
    });
  });
}