import 'package:flutter_test/flutter_test.dart';
import 'package:workout/service/date_time/date_service_now.dart';

void main() {
  group('Date service tests', (){
    test('should return correct date', () async {
      //Given
      final dateService = DateServiceNow();
      //When
      final date = dateService.getToday(DateTime(2021, 1, 12));
      //Then
      expect(date, "2021-01-12");
    });
  });
}
