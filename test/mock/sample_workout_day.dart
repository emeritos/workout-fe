import 'package:workout/feature/workout_day/model/WeekDay.dart';
import "package:workout/feature/workout_day/model/exercise.dart";
import "package:workout/feature/workout_day/model/workout_day.dart";

class SampleWorkoutDay extends WorkoutDay {
  SampleWorkoutDay()
      : super(id: '67800wwaafgfw00', weekDay: WeekDay.MONDAY, exercises: {
          "ABS": [
            Exercise(
                category: "ABS",
                name: "Brzuszki",
                sets: 4,
                reps: 20,
                weight: 24,
                rest: 120,
                order: 1)
          ],
          "LEGS": [
            Exercise(
                category: "LEGS",
                name: "Przysiady",
                sets: 4,
                reps: 20,
                weight: 24,
                rest: 120,
                order: 2)
          ]
        });
}

getExpectedWorkoutDay() => WorkoutDay(id: '67800wwaafgfw00', weekDay: WeekDay.MONDAY, exercises: {
      "ABS": [
        Exercise(
            category: "ABS",
            name: "Brzuszki",
            sets: 4,
            reps: 20,
            weight: 24,
            rest: 120,
            order: 1)
      ],
      "LEGS": [
        Exercise(
            category: "LEGS",
            name: "Przysiady",
            sets: 4,
            reps: 20,
            weight: 24,
            rest: 120,
            order: 2)
      ]
    });

getExpectedGroupExercises() => {
      "ABS": [
        Exercise(
            category: "ABS",
            name: "Brzuszki",
            sets: 4,
            reps: 20,
            weight: 24,
            rest: 120,
            order: 1)
      ],
      "LEGS": [
        Exercise(
            category: "LEGS",
            name: "Przysiady",
            sets: 4,
            reps: 20,
            weight: 24,
            rest: 120,
            order: 2)
      ]
    };
