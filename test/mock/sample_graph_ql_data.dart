Map<String, dynamic> getGraphQlData() => {
  'workoutDay':
    {
      '_id' : "67800wwaafgfw00",
      'dayOfWeek': 1,
      'exercises': [
        {
          'category': 'ABS',
          'name': 'Brzuszki',
          'sets': 4,
          'reps': 20,
          'weight': 24,
          'rest': 120,
          'order': 1
        },
        {
          'category': 'LEGS',
          'name': 'Przysiady',
          'sets': 4,
          'reps': 20,
          'weight': 24,
          'rest': 120,
          'order': 2
        }
      ]
    }
};