import 'package:workout/feature/workout_day/model/exercise.dart';

class SampleExercise extends Exercise {
  SampleExercise({category, name, sets, reps, weight, rest, order})
      : super(
            category: category ?? '1',
            name: name ?? 'TestExercise',
            sets: sets ?? 4,
            reps: reps ?? 10,
            weight: weight ?? 50.0,
            rest: rest ?? 120,
            order: order ?? 1);

}
