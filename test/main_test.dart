import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/main.dart';
import 'package:workout/service/app_localizations/app_localizations_service.dart';

void main() {
  group('Running app tests', () {
    Widget widgetUnderTest;

    setUpAll(() {
      //Given
      configureDependencies(Env.test);
      widgetUnderTest = MaterialApp(
        title: 'Workout app',
        supportedLocales: [
          Locale('en', 'US'),
          Locale('pl', 'PL'),
        ],
        localizationsDelegates: [
          AppLocalizationsDelegate(isTest: true),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: WorkoutApp(),
      );
    });

    testWidgets('should have MaterialApp', (WidgetTester tester) async {
      //When
      await tester.pumpWidget(widgetUnderTest);
      //Then
      expect(find.byType(MaterialApp), findsOneWidget);
    });

    testWidgets('should have Scaffold', (WidgetTester tester) async {
      //When
      await tester.pumpWidget(widgetUnderTest);
      await tester.pumpAndSettle();
      //Then
      expect(find.byType(Scaffold), findsOneWidget);
    });

    testWidgets('should have correct app title', (WidgetTester tester) async {
      //Given
      final String expectedAppTitle = 'Workout app';
      //When
      await tester.pumpWidget(widgetUnderTest);
      MaterialApp actualApp = tester.allWidgets.whereType<MaterialApp>().first;
      //Then
      expect(actualApp.title, expectedAppTitle);
    });
    //TODO fix after finishing #22
    /* testWidgets('should have WorkoutDay', (WidgetTester tester) async {
    //When
    await tester.pumpWidget(widgetUnderTest);
    await tester.pumpAndSettle();
    //Then
    expect(find.byType(WorkoutDayWidget), findsOneWidget);
  }); */
  });
}
