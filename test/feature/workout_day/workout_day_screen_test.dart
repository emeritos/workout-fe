import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/feature/workout_day/bloc/mock_workout_day_bloc.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/feature/workout_day/repository/mock_workout_day_repository.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';
import 'package:workout/main.dart';
import 'package:workout/service/app_localizations/app_localizations_service.dart';
import 'package:workout/view/exercises/exercises_list_view.dart';
import 'package:workout/view/loading_error.dart';


void main() {
  group('Workout day screen tests', () {
    IWorkoutDayRepository mockWorkoutDayRepository;
    Widget widgetUnderTest;
    WorkoutDayBloc mockWorkoutDayBloc;

    setUpAll(() {
      configureDependencies(Env.test);
      mockWorkoutDayRepository = getIt<IWorkoutDayRepository>();
      mockWorkoutDayBloc = MockWorkoutDayBloc();
      widgetUnderTest = MaterialApp(
        title: 'Workout app',
        supportedLocales: [
          Locale('en', 'US'),
          Locale('pl', 'PL'),
        ],
        localizationsDelegates: [
          AppLocalizationsDelegate(isTest: true),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: BlocProvider(
          create: (context) => mockWorkoutDayBloc,
          child: Scaffold(body: MyHome(mockWorkoutDayBloc)),
        ),
      );
    });

    tearDownAll(() {
      mockWorkoutDayBloc.close();
    });

    testWidgets('should show error when get data from service failed',
            (WidgetTester tester) async {
          //Given
          when(mockWorkoutDayBloc.state).thenReturn(WorkoutDayFailureState());
          //When
          await tester.pumpWidget(widgetUnderTest);
          await tester.pumpAndSettle();
          //Then
          expect(find.byType(LoadingError), findsOneWidget);
        });
//TODO fix test after finishing #22
    /* testWidgets(
      'should show loading progress when data from service are downloading',
      (WidgetTester tester) async {
    await tester.runAsync(() async {
    //Given
    when(mockWorkoutDayBloc.workoutDayRepository)
        .thenReturn(mockWorkoutDayRepository);
    when(mockWorkoutDayBloc.state).thenReturn(WorkoutDayInitialState());
    //When
    await tester.pumpWidget(widgetUnderTest);
    await tester.pumpAndSettle();
    //Then
    expect(find.byType(LoadingProgress), findsOneWidget);
  });
      }); */
    testWidgets('should show exercise list view when get data from service',
            (WidgetTester tester) async {
          //Given
          when(mockWorkoutDayBloc.workoutDayRepository)
              .thenReturn(mockWorkoutDayRepository);
          when(mockWorkoutDayBloc.state).thenReturn(WorkoutDaySuccessState(
            await MockWorkoutDayRepository().getSample(),
          ));
          //When
          await tester.pumpWidget(widgetUnderTest);
          await tester.pumpAndSettle();
          //Then
          expect(find.byType(ExercisesListView), findsOneWidget);
        });

    testWidgets('should reload list when pull to refresh',
            (WidgetTester tester) async {
          //Given
          when(mockWorkoutDayBloc.state).thenReturn(WorkoutDaySuccessState(
            await MockWorkoutDayRepository().getSample(),
          ));
          //When
          await tester.pumpWidget(widgetUnderTest);
          await tester.pumpAndSettle();
          await tester.drag(find.byType(ExercisesListView), Offset(0.0, 200.0));
          await tester.pumpAndSettle();
          //Then
          expect(find.text('ABS'), findsOneWidget);
        });
  });
}