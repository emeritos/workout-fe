import 'package:collection/collection.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mockito/mockito.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/feature/workout_day/mapper/exercises_mapper.dart';
import 'package:workout/feature/workout_day/mapper/mock_exercises_mapper.dart';
import 'package:workout/feature/workout_day/model/WeekDay.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';
import 'package:workout/feature/workout_day/repository/workout_day_graph_ql_repository.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';
import 'package:workout/service/date_time/date_service.dart';
import 'package:workout/service/graph_ql/graph_ql_client.dart';
import 'package:workout/service/graph_ql/mock_graph_ql.dart';

void main() {
  group('Graph QL repository tests', () {
    IWorkoutDayRepository workoutDayRepository;
    IExercisesMapper mockExercisesMapper;
    IDateService mockDateService;
    IGraphQLClient mockGraphQLClient;
    GraphQLClient graphQLClient;
    WorkoutDay expectedWorkoutDay;

    setUpAll(() {
      configureDependencies(Env.test);
      mockExercisesMapper = getIt<IExercisesMapper>();
      mockDateService = getIt<IDateService>();
      mockGraphQLClient = getIt<IGraphQLClient>();
      graphQLClient = MockedQueryQlClient();
      workoutDayRepository = WorkoutDayGraphQlRepository(
          mockDateService, mockExercisesMapper, mockGraphQLClient);
      when(mockGraphQLClient.client).thenReturn(graphQLClient);
      expectedWorkoutDay = WorkoutDay(
          id: '67800wwaafgfw00',
          weekDay: WeekDay.MONDAY,
          exercises: groupBy(
              _mockedExercises(), (Exercise exercise) => exercise.category));
    });

    test('should get values from repository', () async {
      //Given
      when(mockDateService.getToday(any)).thenReturn('2021-03-05');
      when(graphQLClient.query(any))
          .thenAnswer((_) => Future.value(_getMockedQueryResult('workoutDay')));
      //When
      final actualWorkoutDay = await workoutDayRepository.get();
      //Then
      expect(actualWorkoutDay, expectedWorkoutDay);
    });

    test('should set new values to fields after change', () async {
      //Given
      final exercises = _mockedExercises();
      when(graphQLClient.mutate(any)).thenAnswer(
          (_) => Future.value(_getMockedQueryResult('updateWorkoutDay')));
      when(mockExercisesMapper.map(exercises))
          .thenReturn(getExpectedMappedExercises());
      //When
      final actualWorkoutDay =
          await workoutDayRepository.set('67800wwaafgfw00', '1', exercises);
      //Then
      expect(actualWorkoutDay, expectedWorkoutDay);
    });
  });
}

QueryResult _getMockedQueryResult(String key) {
  return QueryResult(source: null, data: {
    '$key': {
      'dayOfWeek': 1,
      '_id': '67800wwaafgfw00',
      'exercises': [
        {
          'category': "1",
          'name': "TestExercise",
          'sets': 4,
          'reps': 10,
          'rest': 120,
          'weight': 50.0,
          'order': 1
        },
        {
          'category': "Plecy",
          'name': "second",
          'sets': 4,
          'reps': 10,
          'rest': 120,
          'weight': 50.0,
          'order': 1,
        }
      ]
    }
  });
}

List<Exercise> _mockedExercises() {
  return [
    Exercise(
        category: "1",
        name: "TestExercise",
        sets: 4,
        reps: 10,
        rest: 120,
        weight: 50.0,
        order: 1),
    Exercise(
      category: "Plecy",
      name: "second",
      sets: 4,
      reps: 10,
      rest: 120,
      weight: 50.0,
      order: 1,
    )
  ];
}
