import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/feature/workout_day/model/WeekDay.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';
import 'package:workout/feature/workout_day/repository/mock_workout_day_repository.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';

Future<void> main() async {
  group('Workout day bloc tests', () {
    IWorkoutDayRepository mockWorkoutDayRepository;
    Future<WorkoutDay> workoutDay;

    setUpAll(() {
      configureDependencies(Env.test);
      mockWorkoutDayRepository = getIt<IWorkoutDayRepository>();
      workoutDay = MockWorkoutDayRepository().getSample();
    });

    blocTest(
      'should emit WorkoutDaySuccessState when download data successful',
      build: () {
        when(mockWorkoutDayRepository.get()).thenAnswer((_) => workoutDay);
        return WorkoutDayBloc(mockWorkoutDayRepository);
      },
      act: (bloc) => bloc.add(WorkoutDayFetch()),
      expect: [isA<WorkoutDayLoadingState>(), isA<WorkoutDaySuccessState>()],
    );

    blocTest(
      'should emit WorkoutDayFailureState when download data unsuccessful',
      build: () {
        when(mockWorkoutDayRepository.get())
            .thenAnswer((_) => Future.error(''));
        return WorkoutDayBloc(mockWorkoutDayRepository);
      },
      act: (bloc) => bloc.add(WorkoutDayFetch()),
      expect: [isA<WorkoutDayLoadingState>(), isA<WorkoutDayFailureState>()],
    );

    blocTest(
      'should emit WorkoutDayNoExercisesState when download data without exercises',
      build: () {
        when(mockWorkoutDayRepository.get()).thenAnswer((_) => Future.value(
            WorkoutDay(weekDay: WeekDay.MONDAY, exercises: {})));
        return WorkoutDayBloc(mockWorkoutDayRepository);
      },
      act: (bloc) => bloc.add(WorkoutDayFetch()),
      expect: [
        isA<WorkoutDayLoadingState>(),
        isA<WorkoutDayNoExercisesState>()
      ],
    );
  });
}
