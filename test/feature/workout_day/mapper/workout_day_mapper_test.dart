import 'package:flutter_test/flutter_test.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:workout/feature/workout_day/mapper/workout_day_mapper.dart';

import '../../../mock/sample_graph_ql_data.dart';
import '../../../mock/sample_workout_day.dart';

void main() {
  group('Workout day mapper tests', () {
    WorkoutDayMapper underTest;

    setUp(() {
      QueryResult result = QueryResult();
      result.data = getGraphQlData();
      underTest = WorkoutDayMapper(result);
    });

    test('should return correct workout day', () async {
      //Given
      final expectedWorkoutDay = getExpectedWorkoutDay();
      //When
      final workoutDay = underTest.getWorkoutDay();
      //Then
      expect(workoutDay, expectedWorkoutDay);
    });

    test('should return correct group exercises', () async {
      //Given
      final expectedGroupExercises = getExpectedGroupExercises();
      //When
      final groupExercises = underTest.getGroupedExercises('workoutDay');
      //Then
      expect(groupExercises, expectedGroupExercises);
    });
  });
}
