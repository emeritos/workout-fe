import 'package:flutter_test/flutter_test.dart';
import 'package:workout/feature/workout_day/mapper/exercises_mapper.dart';
import 'package:workout/feature/workout_day/mapper/standard_exercises_mapper.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';

void main() {
  group('Exercises mapper tests', () {
    IExercisesMapper widgetUnderTest;

    setUp(() {
      widgetUnderTest = StandardExercisesMapper();
    });

    test('should return mapped exercises to string', () async {
      //Given
      final expectedString =
'''[{category: "Plecy" name: "Wiosłowanie hantelką w podporze" sets: 4 reps: 10 rest: 120 weight: 12.5 order: 1 }{category: "Plecy" name: "Wiosłowanie sztangą w skłonie" sets: 4 reps: 10 rest: 120 weight: 32.0 order: 2 }]''';
      final List<Exercise> exercises = [
        Exercise(
          category: 'Plecy',
          name: 'Wiosłowanie hantelką w podporze',
          sets: 4,
          reps: 10,
          rest: 120,
          weight: 12.5,
          order: 1,
        ),
        Exercise(
          category: 'Plecy',
          name: 'Wiosłowanie sztangą w skłonie',
          sets: 4,
          reps: 10,
          rest: 120,
          weight: 32,
          order: 2,
        )
      ];
      //When
      final actual = widgetUnderTest.map(exercises);
      //Then
      expect(actual, equals(expectedString));
    });
  });
}
