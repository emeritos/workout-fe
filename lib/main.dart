import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/service/graph_ql/graph_ql_client.dart';

import 'feature/workout_day/repository/workout_day_repository.dart';
import 'feature/workout_day/workout_day_observer.dart';
import 'feature/workout_day/workout_day_screen.dart';
import 'service/app_localizations/app_localizations_service.dart';

void main() {
  configureDependencies(Env.dev);
  Bloc.observer = WorkoutDayObserver();
  runApp(WorkoutApp());
}

class WorkoutApp extends StatefulWidget {
  @override
  _WorkoutAppState createState() => _WorkoutAppState();
}

class _WorkoutAppState extends State<WorkoutApp> {
  final workoutDayBloc = WorkoutDayBloc(getIt<IWorkoutDayRepository>());

  @override
  Widget build(BuildContext context) {
    workoutDayBloc.add(WorkoutDayFetch());
    return GraphQLProvider(
      client: ValueNotifier(getIt<IGraphQLClient>().client),
      child: MaterialApp(
        title: 'Workout app',
        supportedLocales: [
          Locale('en', 'US'),
          Locale('pl', 'PL'),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode &&
                supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        home: Scaffold(
          body: BlocProvider(
            create: (_) => workoutDayBloc,
            child: MyHome(workoutDayBloc),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    workoutDayBloc.close();
    super.dispose();
  }
}

class MyHome extends StatelessWidget {
  final workoutDayBloc;

  MyHome(this.workoutDayBloc);

  @override
  Widget build(BuildContext context) {
    return WorkoutDayWidget(
      workoutDayBloc: workoutDayBloc,
    );
  }
}
