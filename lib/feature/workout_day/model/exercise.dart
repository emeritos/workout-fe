import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Exercise extends Equatable {
  final String category;
  final String name;
  final int sets;
  final int reps;
  final double weight;
  final int rest; //seconds
  final int order;

  Exercise({this.category,
    this.name,
    this.sets,
    this.reps,
    this.weight,
    this.rest,
    this.order});

  @override
  List<Object> get props => [category, name, sets, reps, weight, rest, order];

  @override
  String toString() {
    return 'Exercise{category: $category, name: $name, sets: $sets, reps: $reps, weight: $weight, rest: $rest, order: $order}';
  }

  Exercise copyWith({category, name, sets, reps, weight, rest, order}) {
    return Exercise(category: category ?? this.category,
        name: name ?? this.name,
        sets: sets ?? this.sets,
        reps: reps ?? this.reps,
        weight: weight ?? this.weight,
        rest: rest ?? this.rest,
        order: order ?? this.order);
  }
}
