import 'package:equatable/equatable.dart';
import 'package:workout/feature/workout_day/model/WeekDay.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';

class WorkoutDay extends Equatable {
  final String id;
  final WeekDay weekDay;
  final Map<String, List<Exercise>> exercises;

  WorkoutDay({this.id, this.weekDay, this.exercises});

  @override
  List<Object> get props => [id, weekDay, exercises];

  @override
  String toString() {
    return 'WorkoutDay{id: $id, weekDay: $weekDay, exercises: $exercises}';
  }

  WorkoutDay copyWith({id, weekDay, exercises}) {
    return WorkoutDay(
        id: id ?? this.id,
        weekDay: weekDay ?? this.weekDay,
        exercises: exercises ?? this.exercises);
  }
}
