import 'package:flutter_bloc/flutter_bloc.dart';

class WorkoutDayObserver extends BlocObserver {
  @override
  void onChange(Cubit cubit, Change change) {
    print('${cubit.runtimeType} $change');
    super.onChange(cubit, change);
  }
}
