part of 'workout_day_bloc.dart';

abstract class WorkoutDayEvent {
  const WorkoutDayEvent();
}

class WorkoutDayFetch extends WorkoutDayEvent {
  const WorkoutDayFetch();
}

class WorkoutDayUpdate extends WorkoutDayEvent {
  final WorkoutDay updatedWorkoutDay;
  const WorkoutDayUpdate(this.updatedWorkoutDay);
}
