import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';

part 'workout_day_event.dart';

part 'workout_day_state.dart';

class WorkoutDayBloc extends Bloc<WorkoutDayEvent, WorkoutDayState> {
  final IWorkoutDayRepository workoutDayRepository;

  WorkoutDayBloc(this.workoutDayRepository) : super(WorkoutDayInitialState());

  @override
  Stream<WorkoutDayState> mapEventToState(WorkoutDayEvent event) async* {
    try {
      if (event is WorkoutDayFetch) {
        yield WorkoutDayLoadingState();
        final workoutDay = await workoutDayRepository.get();
        if (workoutDay.exercises == null || workoutDay.exercises.length == 0) {
          yield WorkoutDayNoExercisesState();
        } else {
          yield WorkoutDaySuccessState(workoutDay);
        }
      } else if (event is WorkoutDayUpdate) {
        yield WorkoutDayLoadingState();
        List<Exercise> exercises = [];
        final id = event.updatedWorkoutDay.id;
        final weekDay = event.updatedWorkoutDay.weekDay;
        event.updatedWorkoutDay.exercises.values.forEach((element) {
          exercises.addAll(element);
        });
        workoutDayRepository.set(id, weekDay.index.toString(), exercises);
        yield WorkoutDaySuccessState(event.updatedWorkoutDay);
      }
    } catch (_) {
      yield WorkoutDayFailureState();
    }
  }
}
