part of 'workout_day_bloc.dart';

class WorkoutDayInitialState extends WorkoutDayState {}

class WorkoutDayLoadingState extends WorkoutDayState {}

class WorkoutDayNoExercisesState extends WorkoutDayState {}

class WorkoutDayFailureState extends WorkoutDayState {
  const WorkoutDayFailureState();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WorkoutDayFailureState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class WorkoutDaySuccessState extends WorkoutDayState {
  final WorkoutDay workoutDay;

  const WorkoutDaySuccessState(this.workoutDay);
}

class WorkoutDayState {
  const WorkoutDayState();
}
