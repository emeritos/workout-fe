import 'package:bloc_test/bloc_test.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';

class MockWorkoutDayBloc extends MockBloc<WorkoutDay>
    implements WorkoutDayBloc {}
