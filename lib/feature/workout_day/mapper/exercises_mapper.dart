import 'package:workout/feature/workout_day/model/exercise.dart';

abstract class IExercisesMapper {
  String map(List<Exercise> exercises);
}
