import 'package:injectable/injectable.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';

import 'exercises_mapper.dart';

@dev
@Injectable(as: IExercisesMapper)
class StandardExercisesMapper implements IExercisesMapper {
  @override
  String map(List<Exercise> exercises) {
    if (exercises.length == 0) {
      return '[]';
    }

    StringBuffer result = StringBuffer();
    result.write('[');
    exercises.forEach((exercise) {
      result.write('{');
      result.write('category: "${exercise.category}" ');
      result.write('name: "${exercise.name}" ');
      result.write('sets: ${exercise.sets} ');
      result.write('reps: ${exercise.reps} ');
      result.write('rest: ${exercise.rest} ');
      result.write('weight: ${exercise.weight} ');
      result.write('order: ${exercise.order} ');
      result.write('}');
    });
    result.write(']');
    return result.toString();
  }
}
