import 'package:injectable/injectable.dart';
import 'package:workout/feature/workout_day/mapper/exercises_mapper.dart';
import 'package:mockito/mockito.dart';

@test
@Injectable(as: IExercisesMapper)
class MockExercisesMapper extends Mock implements IExercisesMapper {}

String getExpectedMappedExercises() {
  return '''[{
category: "1"
name: "TestExercise"
sets: 4
reps: 10
rest: 120
weight: 50.0
order: 1
}{
category: "Plecy"
name: "second"
sets: 4
reps: 10
rest: 120
weight: 50.0
order: 1
}]''';
}
