import 'package:collection/collection.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../model/WeekDay.dart';
import '../model/exercise.dart';
import '../model/workout_day.dart';

class WorkoutDayMapper {
  final QueryResult _result;

  WorkoutDayMapper(this._result);

  Map<String, List<Exercise>> getGroupedExercises(String key) {
    final workoutDayData = _result.data[key];
    return _groupExercises(workoutDayData);
  }

  WorkoutDay getWorkoutDay() {
    final key = 'workoutDay';
    final workoutDayData = _result.data[key];
    return _getWorkoutDay(workoutDayData, key);
  }

  WorkoutDay getUpdateWorkoutDay() {
    final key = 'updateWorkoutDay';
    final updateWorkoutDayData = _result.data[key];
    return _getWorkoutDay(updateWorkoutDayData, key);
  }

  Map<String, List<Exercise>> _groupExercises(dynamic workoutDayGraphQL) {
    List<Exercise> exercises = _getExercises(workoutDayGraphQL['exercises']);
    return groupBy(exercises, (Exercise exercise) => exercise.category);
  }

  List<Exercise> _getExercises(dynamic exercisesGraphQL) {
    List<Exercise> list = [];
    if (exercisesGraphQL != null) {
      exercisesGraphQL.forEach((exerciseGraphQL) => list.add(Exercise(
          category: '${exerciseGraphQL['category']}',
          name: '${exerciseGraphQL['name']}',
          sets: int.parse('${exerciseGraphQL['sets']}'),
          reps: int.parse('${exerciseGraphQL['reps']}'),
          weight: double.parse('${exerciseGraphQL['weight']}'),
          rest: int.parse('${exerciseGraphQL['rest']}'),
          order: int.parse('${exerciseGraphQL['order']}'))));
    }
    return list;
  }

  WorkoutDay _getWorkoutDay(dynamic workoutDayData, String key) {
    final weekDay = WeekDay.values[workoutDayData['dayOfWeek']];
    final id = workoutDayData['_id'];
    return WorkoutDay(
        id: id, weekDay: weekDay, exercises: getGroupedExercises(key));
  }
}
