import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/view/exercises/exercises_list_view.dart';
import 'package:workout/view/exercises/no_exercises_error.dart';
import 'package:workout/view/loading_error.dart';
import 'package:workout/view/loading_progress.dart';

class WorkoutDayWidget extends StatelessWidget {
  final WorkoutDayBloc workoutDayBloc;

  const WorkoutDayWidget({Key key, @required this.workoutDayBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          IconButton(
              onPressed: _fetchExercises,
              icon: Icon(Icons.refresh),
          ),
          BlocBuilder<WorkoutDayBloc, WorkoutDayState>(
            builder: (BuildContext context, WorkoutDayState state) {
              if (state is WorkoutDaySuccessState) {
                return ExercisesListView(state.workoutDay);
              } else if (state is WorkoutDayFailureState) {
                return LoadingError();
              } else if (state is WorkoutDayNoExercisesState) {
                return NoExercisesError();
              } else {
                return LoadingProgress();
              }
            },
          ),
        ],
      ),
    );
  }

  Future<void> _fetchExercises() async {
    workoutDayBloc.add(WorkoutDayFetch());
  }
}
