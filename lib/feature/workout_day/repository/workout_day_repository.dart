import 'package:workout/feature/workout_day/model/exercise.dart';

import '../model/workout_day.dart';

abstract class IWorkoutDayRepository {
  Future<WorkoutDay> get();

  Future<WorkoutDay> set(String id, String weekDay, List<Exercise> exercises);
}
