import 'package:injectable/injectable.dart';
import 'package:workout/feature/workout_day/model/WeekDay.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:mockito/mockito.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';

@test
@Injectable(as: IWorkoutDayRepository)
class MockWorkoutDayRepository extends Mock implements IWorkoutDayRepository {
  Future<WorkoutDay> getSample() async =>
      WorkoutDay(weekDay: WeekDay.MONDAY, exercises: {
        "ABS": [
          Exercise(
              category: 'ABS',
              name: 'Brzuszki',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120)
        ],
        "LEGS": [
          Exercise(
              category: 'LEGS',
              name: 'Przysiady',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120)
        ]
      });

  Future<WorkoutDay> getSample2() async =>
      WorkoutDay(weekDay: WeekDay.TUESDAY, exercises: {
        "ABS": [
          Exercise(
              category: 'ABS',
              name: 'Nogi',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120)
        ],
        "LEGS": [
          Exercise(
              category: 'LEGS',
              name: 'Przysiady',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120)
        ]
      });

  Future<WorkoutDay> getSample3() async =>
      WorkoutDay(weekDay: WeekDay.TUESDAY, exercises: {
        "ABS": [
          Exercise(
              category: 'ABS',
              name: 'Podnoszenie nóg',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120,
              order: 1),
          Exercise(
              category: 'ABS',
              name: 'Brzuszki',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120,
              order: 2),
          Exercise(
              category: 'ABS',
              name: 'Kółko',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120,
              order: 3)
        ],
        "Nogi": [
          Exercise(
              category: 'Nogi',
              name: 'Przysiady',
              sets: 4,
              reps: 20,
              weight: 24,
              rest: 120,
              order: 1)
        ]
      });
}
