import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:workout/feature/workout_day/mapper/exercises_mapper.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';
import 'package:workout/feature/workout_day/repository/workout_day_repository.dart';
import 'package:workout/service/date_time/date_service.dart';
import 'package:workout/service/graph_ql/graph_ql_client.dart';
import 'package:workout/service/graph_ql/queries.dart';

import '../mapper/workout_day_mapper.dart';

@dev
@Injectable(as: IWorkoutDayRepository)
class WorkoutDayGraphQlRepository implements IWorkoutDayRepository {
  final IDateService dateService;
  final IExercisesMapper exercisesMapper;
  final IGraphQLClient graphQl;

  WorkoutDayGraphQlRepository(
      this.dateService, this.exercisesMapper, this.graphQl);

  @override
  Future<WorkoutDay> get() async {
    final date = dateService.getToday(DateTime.now());
    final queryResult = await graphQl.client.query(QueryOptions(
      document: gql(getWorkoutDay(date)),
    ));
    return WorkoutDayMapper(queryResult).getWorkoutDay();
  }

  @override
  Future<WorkoutDay> set(
      String id, String weekDay, List<Exercise> exercises) async {
    final mappedExercises = exercisesMapper.map(exercises);
    final queryResult = await graphQl.client.mutate(MutationOptions(
        document: gql(updateWorkoutDay(id, weekDay, mappedExercises))));
    return WorkoutDayMapper(queryResult).getUpdateWorkoutDay();
  }
}
