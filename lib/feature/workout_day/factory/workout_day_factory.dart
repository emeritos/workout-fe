import 'package:workout/feature/workout_day/model/workout_day.dart';

abstract class IWorkoutDayFactory {
  WorkoutDay get(WorkoutDay workoutDay, Map<String, String> element, String value);
}