import 'package:injectable/injectable.dart';
import 'package:workout/feature/workout_day/factory/workout_day_factory.dart';

import '../model/exercise.dart';
import '../model/workout_day.dart';

@dev
@Injectable(as: IWorkoutDayFactory)
class WorkoutDayUpdateFactory implements IWorkoutDayFactory {
  @override
  WorkoutDay get(
      WorkoutDay workoutDay, Map<String, String> element, String value) {
    final int exerciseNumber = int.parse(element['exerciseNumber']);
    WorkoutDay updatedWorkoutDay = workoutDay.copyWith();
    List<Exercise> exercises = updatedWorkoutDay.exercises[element['category']];
    Exercise exerciseToUpdate = exercises[exerciseNumber];
    Exercise updatedExercise;
    switch (int.parse(element["index"])) {
      case 0:
        updatedExercise = exerciseToUpdate.copyWith(name: value);
        break;
      case 1:
        int sets = int.parse(value);
        updatedExercise = exerciseToUpdate.copyWith(sets: sets);
        break;
      case 2:
        int reps = int.parse(value);
        updatedExercise = exerciseToUpdate.copyWith(reps: reps);
        break;
      case 3:
        double weight = double.parse(value);
        updatedExercise = exerciseToUpdate.copyWith(weight: weight);
        break;
      case 4:
        int rest = int.parse(value);
        updatedExercise = exerciseToUpdate.copyWith(rest: rest);
        break;
      default:
        break;
    }
    exercises[exerciseNumber] = updatedExercise;
    return updatedWorkoutDay;
  }
}
