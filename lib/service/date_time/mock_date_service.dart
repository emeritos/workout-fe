import 'package:injectable/injectable.dart';
import 'package:workout/service/date_time/date_service.dart';
import 'package:mockito/mockito.dart';

@test
@Injectable(as: IDateService)
class MockDateService extends Mock implements IDateService {}
