abstract class IDateService {
  String getToday(DateTime now);
}