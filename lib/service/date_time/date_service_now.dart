import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

import 'date_service.dart';

@dev
@Injectable(as: IDateService)
class DateServiceNow implements IDateService {
  @override
  String getToday(DateTime now) {
    final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    return dateFormat.format(now);
  }
}
