import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:workout/service/graph_ql/graph_ql_client.dart';

@dev
@Injectable(as: IGraphQLClient)
class StandardGraphQLClient implements IGraphQLClient {
  @override
  GraphQLClient client = GraphQLClient(
      link: HttpLink('https://emeritos-workout-be.herokuapp.com/graphql'),
      cache: GraphQLCache());
}
