String getWorkoutDay(String date) => '''
{
workoutDay(date: "$date") {
    _id
    dayOfWeek
    exercises {
      category
      name
      sets
      reps
      weight
      rest
      order
    }
  }
}
''';

String updateWorkoutDay(String id, String weekDay, String exercises) => '''
mutation updateWorkoutDay {
  updateWorkoutDay(
    workoutDay: {
      id: "$id"
      dayOfWeek: $weekDay
      exercises: $exercises
    }
  ) {
    _id
    dayOfWeek
    exercises {
      category
      name
      sets
      reps
      rest
      weight
      order
    }
  }
}
''';
//# Write your query or mutation here
// mutation updateWorkoutDay {
//   updateWorkoutDay(
//     workoutDay: {
//       id: "60267d2b261142aa6c297564"
//       dayOfWeek: 1
//       exercises: [
//         {
//           category: "Plecy"
//           name: "Wiosłowanie hantelką w podporze"
//           sets: 4
//           reps: 10
//           rest: 120
//           weight: 12.5
//           order: 1
//         }
//         {
//           category: "Plecy"
//           name: "Wiosłowanie sztangą w skłonie"
//           sets: 4
//           reps: 10
//           rest: 120
//           weight: 32
//           order: 2
//         }
//       ]
//     }
//   ) {
//     dayOfWeek
//     exercises {
//       category
//       name
//       sets
//       reps
//       rest
//       weight
//       order
//     }
//   }
// }