import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:workout/service/graph_ql/graph_ql_client.dart';
import 'package:mockito/mockito.dart';

@test
@Injectable(as: IGraphQLClient)
class MockGraphQlClient extends Mock implements IGraphQLClient {}
