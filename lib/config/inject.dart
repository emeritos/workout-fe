import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'inject.config.dart';

final getIt = GetIt.instance;
abstract class Env {
  static const dev = 'dev';
  static const test = 'test';
}

@InjectableInit()
void configureDependencies(String env) => $initGetIt(getIt, environment: env);