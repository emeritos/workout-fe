// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../feature/workout_day/factory/workout_day_factory.dart' as _i12;
import '../feature/workout_day/factory/workout_day_update_factory.dart' as _i13;
import '../feature/workout_day/mapper/exercises_mapper.dart' as _i6;
import '../feature/workout_day/mapper/mock_exercises_mapper.dart' as _i7;
import '../feature/workout_day/mapper/standard_exercises_mapper.dart' as _i8;
import '../feature/workout_day/repository/mock_workout_day_repository.dart'
    as _i15;
import '../feature/workout_day/repository/workout_day_graph_ql_repository.dart'
    as _i16;
import '../feature/workout_day/repository/workout_day_repository.dart' as _i14;
import '../service/date_time/date_service.dart' as _i3;
import '../service/date_time/date_service_now.dart' as _i4;
import '../service/date_time/mock_date_service.dart' as _i5;
import '../service/graph_ql/graph_ql_client.dart' as _i9;
import '../service/graph_ql/graphql_api.dart' as _i10;
import '../service/graph_ql/mock_graph_ql_client.dart' as _i11;

const String _dev = 'dev';
const String _test = 'test';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String environment, _i2.EnvironmentFilter environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.IDateService>(() => _i4.DateServiceNow(), registerFor: {_dev});
  gh.factory<_i3.IDateService>(() => _i5.MockDateService(),
      registerFor: {_test});
  gh.factory<_i6.IExercisesMapper>(() => _i7.MockExercisesMapper(),
      registerFor: {_test});
  gh.factory<_i6.IExercisesMapper>(() => _i8.StandardExercisesMapper(),
      registerFor: {_dev});
  gh.factory<_i9.IGraphQLClient>(() => _i10.StandardGraphQLClient(),
      registerFor: {_dev});
  gh.factory<_i9.IGraphQLClient>(() => _i11.MockGraphQlClient(),
      registerFor: {_test});
  gh.factory<_i12.IWorkoutDayFactory>(() => _i13.WorkoutDayUpdateFactory(),
      registerFor: {_dev});
  gh.factory<_i14.IWorkoutDayRepository>(() => _i15.MockWorkoutDayRepository(),
      registerFor: {_test});
  gh.factory<_i14.IWorkoutDayRepository>(
      () => _i16.WorkoutDayGraphQlRepository(get<_i3.IDateService>(),
          get<_i6.IExercisesMapper>(), get<_i9.IGraphQLClient>()),
      registerFor: {_dev});
  return get;
}
