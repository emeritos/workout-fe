import 'package:flutter/material.dart';
import 'package:workout/service/app_localizations/app_localizations_service.dart';

class LoadingProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
          SizedBox(height: 50),
          Text(AppLocalizations.of(context).translate('loadingProgress')),
        ],
      ));
}
