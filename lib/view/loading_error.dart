import 'package:flutter/material.dart';
import 'package:workout/service/app_localizations/app_localizations_service.dart';


class LoadingError extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(AppLocalizations.of(context).translate('loadingError')),
          SizedBox(height: 30),
          RotatedBox(
            quarterTurns: 1,
            child: Text(
              ':(',
              style: TextStyle(fontSize: 50),
            ),
          ),
        ],
      ));
}
