import 'package:flutter/material.dart';

class ExerciseCategoryText extends StatelessWidget {
  final String text;

  const ExerciseCategoryText({this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 25,
      ),
    );
  }
}
