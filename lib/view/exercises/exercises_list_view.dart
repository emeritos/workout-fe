import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';

import 'exercise_category_text.dart';
import 'exercise_table/exercise_table.dart';

class ExercisesListView extends StatefulWidget {
  final WorkoutDay _workoutDay;

  const ExercisesListView(this._workoutDay);

  @override
  _ExercisesListViewState createState() => _ExercisesListViewState();
}

class _ExercisesListViewState extends State<ExercisesListView> {
  @override
  Widget build(BuildContext context) {
    final groupExercises = widget._workoutDay.exercises;
    return Expanded(
      child: ReorderableListView(
        children: [
          for (String category in groupExercises.keys.toList())
            Card(
                key: ValueKey(category),
                elevation: 5,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ExerciseCategoryText(text: category),
                  ExercisesTable(groupExercises[category])
                ])),
        ],
        onReorder: reorder,
      ),
    );
  }

  void reorder(oldIndex, newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final Map<String, List<Exercise>> groupExercises =
        Map.from(widget._workoutDay.exercises);
    final List<String> categories = groupExercises.keys.toList();
    final removedCategory = categories.removeAt(oldIndex);
    categories.insert(newIndex, removedCategory);
    widget._workoutDay.exercises.clear();
    categories.forEach((category) {
      widget._workoutDay.exercises
          .putIfAbsent(category, () => groupExercises[category]);
    });
    BlocProvider.of<WorkoutDayBloc>(context)
        .add(WorkoutDayUpdate(widget._workoutDay));
  }
}
