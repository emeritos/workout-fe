import 'package:flutter/material.dart';
import 'package:workout/service/app_localizations/app_localizations_service.dart';

class ExerciseTableRowHeader extends StatelessWidget {
  final String text;

  ExerciseTableRowHeader({this.text});

  fromDictionary(context, text) =>
      AppLocalizations.of(context).translate(text);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        fromDictionary(context, text),
        style: TextStyle(fontSize: 8, height: 2),
      ),
    );
  }
}
