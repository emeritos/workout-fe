import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:workout/config/inject.dart';
import 'package:workout/feature/workout_day/bloc/workout_day_bloc.dart';
import 'package:workout/feature/workout_day/factory/workout_day_factory.dart';
import 'package:workout/feature/workout_day/model/workout_day.dart';

class ExerciseTableRowCell extends StatelessWidget {
  final String text;
  final FontWeight fontWeight;
  final TextInputType textInputType;
  final String inputFilterRegex;
  final Map<String, String> exerciseRowData;
  final bool enabled;

  ExerciseTableRowCell({
    @required this.text,
    this.fontWeight,
    @required this.textInputType,
    this.inputFilterRegex = '',
    @required this.exerciseRowData,
    this.enabled = true,
  });

  @override
  build(BuildContext context) {
    return Container(
        alignment: AlignmentDirectional.center,
        child: TextFormField(
          maxLines: null,
          enabled: enabled,
          textAlignVertical: TextAlignVertical.center,
          initialValue: text,
          inputFormatters: [
            FilteringTextInputFormatter(
              RegExp(inputFilterRegex),
              allow: true,
            ),
          ],
          decoration: InputDecoration(
            border: InputBorder.none,
          ),
          keyboardType: textInputType,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: fontWeight),
          onFieldSubmitted: (value) {
            if (value != text) {
              WorkoutDaySuccessState workoutDayState =
                  BlocProvider.of<WorkoutDayBloc>(context).state;
              WorkoutDay workoutDay = workoutDayState.workoutDay;
              final updatedWorkoutDay =
                  getIt<IWorkoutDayFactory>().get(workoutDay, exerciseRowData, value);
              BlocProvider.of<WorkoutDayBloc>(context)
                  .add(WorkoutDayUpdate(updatedWorkoutDay));
            }
          },
          onEditingComplete: () => FocusScope.of(context).unfocus(),
        ));
  }
}
