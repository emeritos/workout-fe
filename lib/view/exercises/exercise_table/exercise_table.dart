import 'package:flutter/material.dart';
import 'package:workout/feature/workout_day/model/exercise.dart';
import 'package:workout/view/exercises/exercise_table/exercise_table_row_names.dart';

import 'exercise_table_row_cell.dart';
import 'exercise_table_row_header.dart';

class ExercisesTable extends StatelessWidget {
  final List<Exercise> _exercises;

  const ExercisesTable(this._exercises);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Table(
        columnWidths: {
          0: FlexColumnWidth(3),
          1: FlexColumnWidth(1),
          2: FlexColumnWidth(1),
          3: FlexColumnWidth(1),
          4: FlexColumnWidth(1.5),
        },
        border: TableBorder.all(
          color: Colors.black,
          width: 1,
          style: BorderStyle.none,
        ),
        children: _createTableRows(_exercises),
      ),
    );
  }

  List<TableRow> _createTableRows(List<Exercise> exercises) {
    List<TableRow> rows = [];
    rows.add(_createTableHeader());
    _exercises.sort((a, b) => a.order.compareTo(b.order));
    var exerciseNumber = 0;
    exercises.forEach((exercise) {
      rows.add(TableRow(children: [
        TableCell(
            child: ExerciseTableRowCell(
          text: exercise.name,
          fontWeight: FontWeight.bold,
          textInputType: TextInputType.text,
          inputFilterRegex: r'[a-z]|[A-Z]| ',
          exerciseRowData: {
            "exerciseNumber": '$exerciseNumber',
            "index": '0',
            "category": exercise.category,
          },
          enabled: false,
        )),
        TableCell(
          child: ExerciseTableRowCell(
              text: '${exercise.sets}',
              textInputType: TextInputType.number,
              inputFilterRegex: r'^\d+',
              exerciseRowData: {
                "exerciseNumber": '$exerciseNumber',
                "index": '1',
                "category": exercise.category,
              }),
        ),
        TableCell(
            child: ExerciseTableRowCell(
          text: '${exercise.reps}',
          textInputType: TextInputType.number,
          inputFilterRegex: r'^\d+',
          exerciseRowData: {
            "exerciseNumber": '$exerciseNumber',
            "index": '2',
            "category": exercise.category,
          },
        )),
        TableCell(
            child: ExerciseTableRowCell(
          text: '${exercise.weight}',
          textInputType: TextInputType.number,
          inputFilterRegex: r'^\d+\.?\d{0,2}',
          exerciseRowData: {
            "exerciseNumber": '$exerciseNumber',
            "index": '3',
            "category": exercise.category,
          },
        )),
        TableCell(
            child: ExerciseTableRowCell(
          text: '${exercise.rest}',
          textInputType: TextInputType.number,
          inputFilterRegex: r'^\d+',
          exerciseRowData: {
            "exerciseNumber": '$exerciseNumber',
            "index": '4',
            "category": exercise.category,
          },
        )),
      ]));
      exerciseNumber++;
    });
    return rows;
  }

  TableRow _createTableHeader() {
    return TableRow(children: [
      TableCell(
          child: Center(
              child: GestureDetector(
                  onTap: () {
                    print('tap');
                  },
                  child: Icon(
                    Icons.edit_rounded,
                    size: 15,
                  )))),
      TableCell(
          child: ExerciseTableRowHeader(
              text: ExerciseTableRowNames.exerciseTableRowHeaderSets)),
      TableCell(
          child: ExerciseTableRowHeader(
              text: ExerciseTableRowNames.exerciseTableRowHeaderReps)),
      TableCell(
          child: ExerciseTableRowHeader(
              text: ExerciseTableRowNames.exerciseTableRowHeaderWeight)),
      TableCell(
          child: ExerciseTableRowHeader(
              text: ExerciseTableRowNames.exerciseTableRowHeaderRest)),
    ]);
  }
}
